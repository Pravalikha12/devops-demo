/**
 * Copyright (C) 2020  Gaurav Mishra
 *
 * SPDX-License-Identifier: GPL-3.0+
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cppunit/TestFixture.h>
#include <cppunit/extensions/HelperMacros.h>
#include <json/json.h>

#include "../prime.hpp"

using namespace std;

class jsonTest : public CPPUNIT_NS ::TestFixture
{
    CPPUNIT_TEST_SUITE(jsonTest);
    CPPUNIT_TEST(jsonInputTest);
    CPPUNIT_TEST(jsonOutputTest);

    CPPUNIT_TEST_SUITE_END();

protected:
    void jsonInputTest(void)
    {

        const std::string inputJson1 = "{\"number\": 6}";
        const std::string inputJson2 = "{\"number\": 123352}";

        PrimeDetector p;
        Json::Value actualJson1 = p.readJson(inputJson1);
        Json::Value actualJson2 = p.readJson(inputJson2);

        CPPUNIT_ASSERT_EQUAL(6, actualJson1["number"].asInt());
        CPPUNIT_ASSERT_EQUAL(123352, actualJson2["number"].asInt());
    };

    void jsonOutputTest(void)
    {
        Json::Value outputJson1;
        outputJson1["isPrime"] = true;

        Json::Value outputJson2;
        outputJson2["isPrime"] = false;

        const std::string expectedOutput1 = "{\n   \"isPrime\" : true\n}\n";
        const std::string expectedOutput2 = "{\n   \"isPrime\" : false\n}\n";

        PrimeDetector p;
        std::string actualJson1 = p.printJson(outputJson1);
        std::string actualJson2 = p.printJson(outputJson2);

        CPPUNIT_ASSERT_EQUAL(expectedOutput1, actualJson1);
        CPPUNIT_ASSERT_EQUAL(expectedOutput2, actualJson2);
    };
};

CPPUNIT_TEST_SUITE_REGISTRATION(jsonTest);

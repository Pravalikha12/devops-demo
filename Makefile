# Copyright (C) 2020  Gaurav Mishra
#
# SPDX-License-Identifier: GPL-3.0+
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

STATIC?=

all: primejson

primejson: prime.o main.cpp
	$(CXX) ${STATIC} prime.o $(shell pkg-config --libs jsoncpp) main.cpp -o $@

prime.o: prime.hpp prime.cpp
	$(CXX) -c $(shell pkg-config --cflags jsoncpp) prime.hpp prime.cpp

test:
	$(MAKE) -C test test

clean:
	rm -f primejson prime.o prime.hpp.gch
	$(MAKE) -C test clean

.PHONY: clean test
